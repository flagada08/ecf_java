package com.sparadrap.app.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import com.sparadrap.app.exception.PharmaException;
import com.sparadrap.app.model.Medecin;
import com.sparadrap.app.model.Medicament;
import com.sparadrap.app.model.Patient;
import com.sparadrap.app.model.Specialite;

class PatientTest {

	@Test
	void testNumSecuSocial() throws PharmaException {
		Medecin medecin1 = new Medecin(
				"testNomMedecin1", 
				"testPrenomMedecin1", 
				1, 
				"rue du test", 
				12345, 
				"testCity1", 
				1234567890, 
				"testMedecin1@test.st", 
				12345678, 
				null, 
				null, 
				Specialite.GYNECO
			);
		Patient patient1 = new Patient(
				"testNomPatient1", 
				"testPrenomPatient1", 
				1, 
				"rue du test", 
				12345, 
				"testCity1", 
				1234567890, 
				"testPatient1@test.st", 
				"31/12/1234", 
				1234567890123456L, 
				null, 
				medecin1,
				null,
				new ArrayList<Medicament>()
			);
		assertEquals(1234567890123456L, patient1.getNumSecuSocial());
		try
		{patient1.setMedecin(null);
		fail("Le medecin n'est pas null");
		}
		catch (PharmaException pe){
			assert(pe.getMessage().contains("un médecin ne peut pas être null"));
		}
	}


	@Test
	void testGetMutuelle() {
		fail("Not yet implemented");
	}

	@Test
	void testSetMutuelle() {
		fail("Not yet implemented");
	}

	@Test
	void testGetMedecin() {
		fail("Not yet implemented");
	}

	@Test
	void testSetMedecin() {
		fail("Not yet implemented");
	}

	@Test
	void testGetOrdonnance() {
		fail("Not yet implemented");
	}

	@Test
	void testSetOrdonnance() {
		fail("Not yet implemented");
	}

}
